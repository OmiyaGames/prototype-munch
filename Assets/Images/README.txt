Images:
Where all the image assets used in a 2D context (such as GUI) are stored.
If an image is not used in a model, it goes here!
